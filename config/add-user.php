#!/usr/bin/env php
<?php
/**
 * This file is part of the Zendy CMS package.
 *
 * (c) Concetto Vecchio <info@cvsolutions.it>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Dotenv\Dotenv;
use SimpleSkeletonCMS\Entity\User;
use SimpleSkeletonCMS\Utility\Colors;
use SimpleSkeletonCMS\Utility\Messages;
use Slim\App;
use Slim\Container;

require __DIR__ . '/../vendor/autoload.php';

try {
    if (PHP_SAPI != 'cli') {
        exit();
    }
    $dotenv = new Dotenv(__DIR__ . '/../');
    $dotenv->load();

    $settings  = require __DIR__ . '/settings.php';
    $container = new Container($settings);
    $app       = new App($container);
    $today     = new DateTime();
    $colors    = new Colors();
    $hash      = password_hash('pass', PASSWORD_DEFAULT, ['cost' => 12]);

    $user = new User();
    $user->setFullname('Concetto Vecchio');
    $user->setUsermail('info@cvsolutions.it');
    $user->setPwd($hash);
    $user->setRole(1);
    $user->setActive(1);
    $user->setLastAccess($today);
    $user->setRegistration($today);

    /** @var \Doctrine\ORM\EntityManager $em */
    $em = $app->getContainer()->get('entityManager');
    $em->persist($user);
    $em->flush();
    echo $colors->getColoredString(Messages::MESSAGE_CONFIRMATION, 'green');
} catch (Exception $exception) {
    echo $colors->getColoredString($exception->getMessage(), 'red');
}
