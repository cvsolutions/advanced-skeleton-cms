<?php
/**
 * This file is part of the Zendy CMS package.
 *
 * (c) Concetto Vecchio <info@cvsolutions.it>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Slim\Container;

return [
    'settings'      => [
        'displayErrorDetails'               => true,
        'determineRouteBeforeAppMiddleware' => true,
    ],
    'doctrine'      => [
        'meta'       => [
            'entity_path'           => [
                __DIR__ . '/../src/Entity',
            ],
            'auto_generate_proxies' => true,
            'proxy_dir'             => __DIR__ . '/../cache',
            'cache'                 => null,
        ],
        'connection' => [
            'driver'   => getenv('DB_CONNECTION'),
            'host'     => getenv('DB_HOST'),
            'dbname'   => getenv('DB_DATABASE'),
            'user'     => getenv('DB_USERNAME'),
            'password' => getenv('DB_PASSWORD'),
        ],
    ],
    'entityManager' => function (Container $c) {
        $settings = $c->get('doctrine');
        $config   = Setup::createAnnotationMetadataConfiguration(
            $settings['meta']['entity_path'],
            $settings['meta']['auto_generate_proxies'],
            $settings['meta']['proxy_dir'],
            $settings['meta']['cache'],
            false
        );
        return EntityManager::create($settings['connection'], $config);
    },
    'smarty'        => function () {
        $smarty = new Smarty();
        $smarty->setCompileDir(__DIR__ . '/../cache');
        $smarty->addPluginsDir([__DIR__ . '/../templates/plugins']);
        $smarty->addTemplateDir([
            'administrator' => __DIR__ . '/../templates/administrator',
            'application'   => __DIR__ . '/../templates/application',
        ]);
        return $smarty;
    },
];
