<?php
/**
 * This file is part of the Zendy CMS package.
 *
 * (c) Concetto Vecchio <info@cvsolutions.it>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Doctrine\ORM\Tools\Setup;
use Dotenv\Dotenv;

require __DIR__ . '/../vendor/autoload.php';

if (PHP_SAPI != 'cli') {
    exit();
}

$dotenv = new Dotenv(__DIR__ . '/../');
$dotenv->load();

$settings = include __DIR__ . '/settings.php';
$config   = Setup::createAnnotationMetadataConfiguration(
    $settings['doctrine']['meta']['entity_path'],
    $settings['doctrine']['meta']['auto_generate_proxies'],
    $settings['doctrine']['meta']['proxy_dir'],
    $settings['doctrine']['meta']['cache'],
    false
);
$em       = EntityManager::create($settings['doctrine']['connection'], $config);
return ConsoleRunner::createHelperSet($em);
