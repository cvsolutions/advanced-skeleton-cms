<?php
/**
 * This file is part of the Zendy CMS package.
 *
 * (c) Concetto Vecchio <info@cvsolutions.it>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace SimpleSkeletonCMS\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class User
 * @package SimpleSkeletonCMS\Entity
 * @ORM\Entity()
 * @ORM\Table(
 *     name="cms_users",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="cms_users_usermail",columns={"usermail"})}
 * )
 */
class User
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="fullname", type="string", length=255, nullable=false)
     */
    protected $fullname;

    /**
     * @var string
     * @ORM\Column(name="usermail", type="string", length=255, nullable=false)
     */
    protected $usermail;

    /**
     * @var string
     * @ORM\Column(name="pwd", type="string", length=255, nullable=false)
     */
    protected $pwd;

    /**
     * @var string
     * @ORM\Column(name="role", type="string", length=255, nullable=false)
     */
    protected $role;

    /**
     * @var boolean
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    protected $active;

    /**
     * @var \DateTime
     * @ORM\Column(name="last_access", type="datetime", nullable=false)
     */
    protected $lastAccess;

    /**
     * @var \DateTime
     * @ORM\Column(name="registration", type="datetime", nullable=false)
     */
    protected $registration;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return User
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * @param string $fullname
     * @return User
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsermail()
    {
        return $this->usermail;
    }

    /**
     * @param string $usermail
     * @return User
     */
    public function setUsermail($usermail)
    {
        $this->usermail = $usermail;
        return $this;
    }

    /**
     * @return string
     */
    public function getPwd()
    {
        return $this->pwd;
    }

    /**
     * @param string $pwd
     * @return User
     */
    public function setPwd($pwd)
    {
        $this->pwd = $pwd;
        return $this;
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param string $role
     * @return User
     */
    public function setRole($role)
    {
        $this->role = $role;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return User
     */
    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getLastAccess()
    {
        return $this->lastAccess;
    }

    /**
     * @param \DateTime $lastAccess
     * @return User
     */
    public function setLastAccess($lastAccess)
    {
        $this->lastAccess = $lastAccess;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getRegistration()
    {
        return $this->registration;
    }

    /**
     * @param \DateTime $registration
     * @return User
     */
    public function setRegistration($registration)
    {
        $this->registration = $registration;
        return $this;
    }
}
