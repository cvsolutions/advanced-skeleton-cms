<?php
/**
 * This file is part of the Zendy CMS package.
 *
 * (c) Concetto Vecchio <info@cvsolutions.it>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SimpleSkeletonCMS\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Page
 * @package SimpleSkeletonCMS\Entity
 * @ORM\Entity(repositoryClass="SimpleSkeletonCMS\Repository\PageRepository")
 * @ORM\Table(name="cms_pages")
 */
class Page
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     * @ORM\Column(name="parent", type="integer", nullable=true)
     */
    protected $parent;

    /**
     * @var string
     * @ORM\Column(name="title_it", type="string", length=255, nullable=false)
     */
    protected $titleIt;

    /**
     * @var string
     * @ORM\Column(name="slug_it", type="string", length=255, nullable=false)
     */
    protected $slugIt;

    /**
     * @var string
     * @ORM\Column(name="content_it", type="text", nullable=true)
     */
    protected $contentIt;

    /**
     * @var string
     * @ORM\Column(name="meta_title_it", type="text", nullable=true)
     */
    protected $metaTitleIt;

    /**
     * @var string
     * @ORM\Column(name="meta_description_it", type="text", nullable=true)
     */
    protected $metaDescriptionIt;

    /**
     * @var string
     * @ORM\Column(name="meta_keywords_it", type="text", nullable=true)
     */
    protected $metaKeywordsIt;

    /**
     * @var string
     * @ORM\Column(name="title_en", type="string", length=255, nullable=true)
     */
    protected $titleEn;

    /**
     * @var string
     * @ORM\Column(name="slug_en", type="string", length=255, nullable=true)
     */
    protected $slugEn;

    /**
     * @var string
     * @ORM\Column(name="content_en", type="text", nullable=true)
     */
    protected $contentEn;

    /**
     * @var string
     * @ORM\Column(name="meta_title_en", type="text", nullable=true)
     */
    protected $metaTitleEn;

    /**
     * @var string
     * @ORM\Column(name="meta_description_en", type="text", nullable=true)
     */
    protected $metaDescriptionEn;

    /**
     * @var string
     * @ORM\Column(name="meta_keywords_en", type="text", nullable=true)
     */
    protected $metaKeywordsEn;


    /**
     * @var string
     * @ORM\Column(name="title_de", type="string", length=255, nullable=true)
     */
    protected $titleDe;

    /**
     * @var string
     * @ORM\Column(name="slug_de", type="string", length=255, nullable=true)
     */
    protected $slugDe;

    /**
     * @var string
     * @ORM\Column(name="content_de", type="text", nullable=true)
     */
    protected $contentDe;

    /**
     * @var string
     * @ORM\Column(name="meta_title_de", type="text", nullable=true)
     */
    protected $metaTitleDe;

    /**
     * @var string
     * @ORM\Column(name="meta_description_de", type="text", nullable=true)
     */
    protected $metaDescriptionDe;

    /**
     * @var string
     * @ORM\Column(name="meta_keywords_de", type="text", nullable=true)
     */
    protected $metaKeywordsDe;

    /**
     * @var string
     * @ORM\Column(name="title_fr", type="string", length=255, nullable=true)
     */
    protected $titleFr;

    /**
     * @var string
     * @ORM\Column(name="slug_fr", type="string", length=255, nullable=true)
     */
    protected $slugFr;

    /**
     * @var string
     * @ORM\Column(name="content_fr", type="text", nullable=true)
     */
    protected $contentFr;

    /**
     * @var string
     * @ORM\Column(name="meta_title_fr", type="text", nullable=true)
     */
    protected $metaTitleFr;

    /**
     * @var string
     * @ORM\Column(name="meta_description_fr", type="text", nullable=true)
     */
    protected $metaDescriptionFr;

    /**
     * @var string
     * @ORM\Column(name="meta_keywords_fr", type="text", nullable=true)
     */
    protected $metaKeywordsFr;

    /**
     * @var string
     * @ORM\Column(name="title_es", type="string", length=255, nullable=true)
     */
    protected $titleEs;

    /**
     * @var string
     * @ORM\Column(name="slug_es", type="string", length=255, nullable=true)
     */
    protected $slugEs;

    /**
     * @var string
     * @ORM\Column(name="content_es", type="text", nullable=true)
     */
    protected $contentEs;

    /**
     * @var string
     * @ORM\Column(name="meta_title_es", type="text", nullable=true)
     */
    protected $metaTitleEs;

    /**
     * @var string
     * @ORM\Column(name="meta_description_es", type="text", nullable=true)
     */
    protected $metaDescriptionEs;

    /**
     * @var string
     * @ORM\Column(name="meta_keywords_es", type="text", nullable=true)
     */
    protected $metaKeywordsEs;

    /**
     * @var string
     * @ORM\Column(name="link_external", type="string", length=255, nullable=true)
     */
    protected $linkExternal;

    /**
     * @var bool
     * @ORM\Column(name="home_page", type="boolean", nullable=true)
     */
    protected $homePage;

    /**
     * @var \DateTime
     * @ORM\Column(name="publication", type="datetime", nullable=true)
     */
    protected $publication;

    /**
     * @var \DateTime
     * @ORM\Column(name="modification", type="datetime", nullable=true)
     */
    protected $modification;

    /**
     * @var int
     * @ORM\Column(name="sorting", type="integer", nullable=true)
     */
    protected $sorting;

    /**
     * @var bool
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    protected $active;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Page
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param int $parent
     * @return Page
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitleIt()
    {
        return $this->titleIt;
    }

    /**
     * @param string $titleIt
     * @return Page
     */
    public function setTitleIt($titleIt)
    {
        $this->titleIt = $titleIt;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlugIt()
    {
        return $this->slugIt;
    }

    /**
     * @param string $slugIt
     * @return Page
     */
    public function setSlugIt($slugIt)
    {
        $this->slugIt = $slugIt;
        return $this;
    }

    /**
     * @return string
     */
    public function getContentIt()
    {
        return $this->contentIt;
    }

    /**
     * @param string $contentIt
     * @return Page
     */
    public function setContentIt($contentIt)
    {
        $this->contentIt = $contentIt;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaTitleIt()
    {
        return $this->metaTitleIt;
    }

    /**
     * @param string $metaTitleIt
     * @return Page
     */
    public function setMetaTitleIt($metaTitleIt)
    {
        $this->metaTitleIt = $metaTitleIt;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaDescriptionIt()
    {
        return $this->metaDescriptionIt;
    }

    /**
     * @param string $metaDescriptionIt
     * @return Page
     */
    public function setMetaDescriptionIt($metaDescriptionIt)
    {
        $this->metaDescriptionIt = $metaDescriptionIt;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaKeywordsIt()
    {
        return $this->metaKeywordsIt;
    }

    /**
     * @param string $metaKeywordsIt
     * @return Page
     */
    public function setMetaKeywordsIt($metaKeywordsIt)
    {
        $this->metaKeywordsIt = $metaKeywordsIt;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitleEn()
    {
        return $this->titleEn;
    }

    /**
     * @param string $titleEn
     * @return Page
     */
    public function setTitleEn($titleEn)
    {
        $this->titleEn = $titleEn;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlugEn()
    {
        return $this->slugEn;
    }

    /**
     * @param string $slugEn
     * @return Page
     */
    public function setSlugEn($slugEn)
    {
        $this->slugEn = $slugEn;
        return $this;
    }

    /**
     * @return string
     */
    public function getContentEn()
    {
        return $this->contentEn;
    }

    /**
     * @param string $contentEn
     * @return Page
     */
    public function setContentEn($contentEn)
    {
        $this->contentEn = $contentEn;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaTitleEn()
    {
        return $this->metaTitleEn;
    }

    /**
     * @param string $metaTitleEn
     * @return Page
     */
    public function setMetaTitleEn($metaTitleEn)
    {
        $this->metaTitleEn = $metaTitleEn;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaDescriptionEn()
    {
        return $this->metaDescriptionEn;
    }

    /**
     * @param string $metaDescriptionEn
     * @return Page
     */
    public function setMetaDescriptionEn($metaDescriptionEn)
    {
        $this->metaDescriptionEn = $metaDescriptionEn;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaKeywordsEn()
    {
        return $this->metaKeywordsEn;
    }

    /**
     * @param string $metaKeywordsEn
     * @return Page
     */
    public function setMetaKeywordsEn($metaKeywordsEn)
    {
        $this->metaKeywordsEn = $metaKeywordsEn;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitleDe()
    {
        return $this->titleDe;
    }

    /**
     * @param string $titleDe
     * @return Page
     */
    public function setTitleDe($titleDe)
    {
        $this->titleDe = $titleDe;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlugDe()
    {
        return $this->slugDe;
    }

    /**
     * @param string $slugDe
     * @return Page
     */
    public function setSlugDe($slugDe)
    {
        $this->slugDe = $slugDe;
        return $this;
    }

    /**
     * @return string
     */
    public function getContentDe()
    {
        return $this->contentDe;
    }

    /**
     * @param string $contentDe
     * @return Page
     */
    public function setContentDe($contentDe)
    {
        $this->contentDe = $contentDe;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaTitleDe()
    {
        return $this->metaTitleDe;
    }

    /**
     * @param string $metaTitleDe
     * @return Page
     */
    public function setMetaTitleDe($metaTitleDe)
    {
        $this->metaTitleDe = $metaTitleDe;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaDescriptionDe()
    {
        return $this->metaDescriptionDe;
    }

    /**
     * @param string $metaDescriptionDe
     * @return Page
     */
    public function setMetaDescriptionDe($metaDescriptionDe)
    {
        $this->metaDescriptionDe = $metaDescriptionDe;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaKeywordsDe()
    {
        return $this->metaKeywordsDe;
    }

    /**
     * @param string $metaKeywordsDe
     * @return Page
     */
    public function setMetaKeywordsDe($metaKeywordsDe)
    {
        $this->metaKeywordsDe = $metaKeywordsDe;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitleFr()
    {
        return $this->titleFr;
    }

    /**
     * @param string $titleFr
     * @return Page
     */
    public function setTitleFr($titleFr)
    {
        $this->titleFr = $titleFr;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlugFr()
    {
        return $this->slugFr;
    }

    /**
     * @param string $slugFr
     * @return Page
     */
    public function setSlugFr($slugFr)
    {
        $this->slugFr = $slugFr;
        return $this;
    }

    /**
     * @return string
     */
    public function getContentFr()
    {
        return $this->contentFr;
    }

    /**
     * @param string $contentFr
     * @return Page
     */
    public function setContentFr($contentFr)
    {
        $this->contentFr = $contentFr;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaTitleFr()
    {
        return $this->metaTitleFr;
    }

    /**
     * @param string $metaTitleFr
     * @return Page
     */
    public function setMetaTitleFr($metaTitleFr)
    {
        $this->metaTitleFr = $metaTitleFr;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaDescriptionFr()
    {
        return $this->metaDescriptionFr;
    }

    /**
     * @param string $metaDescriptionFr
     * @return Page
     */
    public function setMetaDescriptionFr($metaDescriptionFr)
    {
        $this->metaDescriptionFr = $metaDescriptionFr;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaKeywordsFr()
    {
        return $this->metaKeywordsFr;
    }

    /**
     * @param string $metaKeywordsFr
     * @return Page
     */
    public function setMetaKeywordsFr($metaKeywordsFr)
    {
        $this->metaKeywordsFr = $metaKeywordsFr;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitleEs()
    {
        return $this->titleEs;
    }

    /**
     * @param string $titleEs
     * @return Page
     */
    public function setTitleEs($titleEs)
    {
        $this->titleEs = $titleEs;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlugEs()
    {
        return $this->slugEs;
    }

    /**
     * @param string $slugEs
     * @return Page
     */
    public function setSlugEs($slugEs)
    {
        $this->slugEs = $slugEs;
        return $this;
    }

    /**
     * @return string
     */
    public function getContentEs()
    {
        return $this->contentEs;
    }

    /**
     * @param string $contentEs
     * @return Page
     */
    public function setContentEs($contentEs)
    {
        $this->contentEs = $contentEs;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaTitleEs()
    {
        return $this->metaTitleEs;
    }

    /**
     * @param string $metaTitleEs
     * @return Page
     */
    public function setMetaTitleEs($metaTitleEs)
    {
        $this->metaTitleEs = $metaTitleEs;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaDescriptionEs()
    {
        return $this->metaDescriptionEs;
    }

    /**
     * @param string $metaDescriptionEs
     * @return Page
     */
    public function setMetaDescriptionEs($metaDescriptionEs)
    {
        $this->metaDescriptionEs = $metaDescriptionEs;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaKeywordsEs()
    {
        return $this->metaKeywordsEs;
    }

    /**
     * @param string $metaKeywordsEs
     * @return Page
     */
    public function setMetaKeywordsEs($metaKeywordsEs)
    {
        $this->metaKeywordsEs = $metaKeywordsEs;
        return $this;
    }

    /**
     * @return string
     */
    public function getLinkExternal()
    {
        return $this->linkExternal;
    }

    /**
     * @param string $linkExternal
     * @return Page
     */
    public function setLinkExternal($linkExternal)
    {
        $this->linkExternal = $linkExternal;
        return $this;
    }

    /**
     * @return bool
     */
    public function isHomePage()
    {
        return $this->homePage;
    }

    /**
     * @param bool $homePage
     * @return Page
     */
    public function setHomePage($homePage)
    {
        $this->homePage = $homePage;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPublication()
    {
        return $this->publication;
    }

    /**
     * @param \DateTime $publication
     * @return Page
     */
    public function setPublication($publication)
    {
        $this->publication = $publication;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getModification()
    {
        return $this->modification;
    }

    /**
     * @param \DateTime $modification
     * @return Page
     */
    public function setModification($modification)
    {
        $this->modification = $modification;
        return $this;
    }

    /**
     * @return int
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * @param int $sorting
     * @return Page
     */
    public function setSorting($sorting)
    {
        $this->sorting = $sorting;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return Page
     */
    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }
}
