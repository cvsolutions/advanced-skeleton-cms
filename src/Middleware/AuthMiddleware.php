<?php
/**
 * This file is part of the Zendy CMS package.
 *
 * (c) Concetto Vecchio <info@cvsolutions.it>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SimpleSkeletonCMS\Middleware;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use SimpleSkeletonCMS\Utility\Messages;
use SimpleSkeletonCMS\Utility\Session;

/**
 * Class AuthMiddleware
 * @package SimpleSkeletonCMS\Middleware
 */
class AuthMiddleware
{
    /**
     * @param Request $request
     * @param Response $response
     * @param callable $next
     * @return Response
     */
    public function __invoke(Request $request, Response $response, callable $next)
    {
        $session = new Session();
        if (empty($session->get('Authentication'))) {
            $response->withStatus(404);
            $response->withHeader('Content-Type', 'text/html');
            $response->getBody()->write(Messages::MESSAGE_403);
            return $response;
        }
        $response = $next($request, $response);
        return $response;
    }
}
