<?php
/**
 * This file is part of the Zendy CMS package.
 *
 * (c) Concetto Vecchio <info@cvsolutions.it>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SimpleSkeletonCMS\Middleware;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class SessionMiddleware
 * @package SimpleSkeletonCMS\Middleware
 */
class SessionMiddleware
{
    /**
     * @var array
     */
    protected $options = [
        'name'          => 'ZendyCMS',
        'lifetime'      => 7200,
        'path'          => null,
        'domain'        => null,
        'secure'        => false,
        'httponly'      => true,
        'cache_limiter' => 'nocache',
    ];

    /**
     * SessionMiddleware constructor.
     * @param array $options
     */
    public function __construct($options = [])
    {
        $keys = array_keys($this->options);
        foreach ($keys as $key) {
            if (array_key_exists($key, $options)) {
                $this->options[$key] = $options[$key];
            }
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param callable $next
     * @return mixed
     */
    public function __invoke(Request $request, Response $response, callable $next)
    {
        $this->start();
        return $next($request, $response);
    }

    /**
     * session start
     */
    public function start()
    {
        if (session_status() == PHP_SESSION_ACTIVE) {
            return;
        }
        $options  = $this->options;
        $current  = session_get_cookie_params();
        $lifetime = (int)($options['lifetime'] ?: $current['lifetime']);
        $path     = $options['path'] ?: $current['path'];
        $domain   = $options['domain'] ?: $current['domain'];
        $secure   = (bool)$options['secure'];
        $httponly = (bool)$options['httponly'];
        session_set_cookie_params($lifetime, $path, $domain, $secure, $httponly);
        session_name($options['name']);
        session_cache_limiter($options['cache_limiter']);
        session_start();
    }
}
