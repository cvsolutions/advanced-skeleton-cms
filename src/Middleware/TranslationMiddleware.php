<?php
/**
 * This file is part of the Zendy CMS package.
 *
 * (c) Concetto Vecchio <info@cvsolutions.it>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SimpleSkeletonCMS\Middleware;

use Interop\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use SimpleSkeletonCMS\Utility\Messages;
use smmoosavi\util\gettext\L10n;

/**
 * Class TranslationMiddleware
 * @package SimpleSkeletonCMS\Middleware
 */
class TranslationMiddleware
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var string
     */
    private $defaultLang = 'it';

    /**
     * @var array
     */
    private $languagesAccepted = ['it', 'en', 'de', 'fr', 'es'];

    /**
     * TranslationMiddleware constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param callable $next
     * @return Response
     */
    public function __invoke(Request $request, Response $response, callable $next)
    {
        $route = $request->getAttribute('route');

        if (is_null($route)) {
            $response->withStatus(404);
            $response->withHeader('Content-Type', 'text/html');
            $response->getBody()->write(Messages::MESSAGE_404);
            return $response;
        }

        $arguments = $route->getArguments();
        $lang      = isset($arguments['lang']) ? $arguments['lang'] : $this->defaultLang;

        if (!in_array($lang, $this->languagesAccepted)) {
            $response->withStatus(404);
            $response->withHeader('Content-Type', 'text/html');
            $response->getBody()->write(Messages::MESSAGE_404);
            return $response;
        }

        $smarty = $this->container->get('smarty');
        $smarty->assign('lang', $lang);

        L10n::init($lang, __DIR__ . '/../../language/' . $lang . '.mo');
        $response = $next($request, $response);
        return $response;
    }
}
