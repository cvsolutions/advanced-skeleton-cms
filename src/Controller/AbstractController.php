<?php
/**
 * This file is part of the Zendy CMS package.
 *
 * (c) Concetto Vecchio <info@cvsolutions.it>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SimpleSkeletonCMS\Controller;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use SimpleSkeletonCMS\Entity\Page;
use SimpleSkeletonCMS\Entity\User;
use SimpleSkeletonCMS\Utility\FlashMessages;
use SimpleSkeletonCMS\Utility\Session;

/**
 * Class AbstractController
 * @package SimpleSkeletonCMS\Controller
 */
abstract class AbstractController
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * AbstractController constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param $tpl
     * @param array $assign
     * @return mixed
     */
    public function smarty($tpl, $assign = [])
    {
        $smarty = $this->container->get('smarty');
        if (count($assign) > 0) {
            foreach ($assign as $key => $value) {
                $smarty->assign($key, $value);
            }
        }
        $smarty->display($tpl);
        return $smarty;
    }

    /**
     * @return EntityManager
     */
    public function entityManager()
    {
        return $this->container->get('entityManager');
    }

    /**
     * @return FlashMessages
     */
    public function flash()
    {
        return new FlashMessages();
    }

    /**
     * @return Session
     */
    public function session()
    {
        return new Session();
    }

    /**
     * @return User
     */
    public function user()
    {
        $session = $this->session();
        $auth    = $session->get('Authentication');
        /** @var User $row */
        $row = $this->entityManager()->getRepository(User::class)->find($auth['id']);
        return $row;
    }

    /**
     * @param int $parent
     * @param int $level
     * @param string $lang
     * @return array
     */
    public function navbar($parent = 0, $level = 0, $lang = '')
    {
        $menu = [];
        foreach ($this->entityManager()->getRepository(Page::class)->navbar($parent, $lang) as $row) {
            $permalink = $row['link_external']
                ? $row['link_external']
                : sprintf('/%s/%s', $lang, $row['slug_' . $lang]);
            if ($row['Count'] > 0) {
                $menu[] = [
                    'name'      => $row['title_' . $lang],
                    'permalink' => $permalink,
                    'children'  => $this->navbar($row['id'], ($level + 1), $lang),
                ];
            } elseif ($row['Count'] == 0) {
                $menu[] = [
                    'name'      => $row['title_' . $lang],
                    'permalink' => $permalink,
                    'children'  => [],
                ];
            };
        }
        return $menu;
    }
}
