<?php
/**
 * This file is part of the Zendy CMS package.
 *
 * (c) Concetto Vecchio <info@cvsolutions.it>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SimpleSkeletonCMS\Controller\Administrator;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use SimpleSkeletonCMS\Controller\AbstractController;

/**
 * Class LogoutController
 * @package SimpleSkeletonCMS\Controller\Administrator
 */
class LogoutController extends AbstractController
{
    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function index(Request $request, Response $response, $args)
    {
        $this->session()->destroy();
        return $response->withStatus(302)->withHeader('Location', '/');
    }
}
