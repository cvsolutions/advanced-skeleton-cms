<?php
/**
 * This file is part of the Zendy CMS package.
 *
 * (c) Concetto Vecchio <info@cvsolutions.it>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SimpleSkeletonCMS\Controller\Administrator;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use SimpleSkeletonCMS\Controller\AbstractController;
use SimpleSkeletonCMS\Entity\User;
use SimpleSkeletonCMS\Utility\Messages;
use SimpleSkeletonCMS\Utility\NoCSRF;

/**
 * Class LoginController
 * @package SimpleSkeletonCMS\Controller\Administrator
 */
class LoginController extends AbstractController
{
    const SUCCESS_REDIRECT = '/admin/dashboard';

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return \Smarty|Response
     */
    public function index(Request $request, Response $response, $args)
    {
        if (!empty($this->session()->get('Authentication'))) {
            return $response->withStatus(302)->withHeader('Location', self::SUCCESS_REDIRECT);
        }
        $msg = $this->flash();
        $em  = $this->entityManager();
        if ($request->getMethod() == 'POST') {
            $form     = $request->getParsedBody();
            $usermail = trim($form['usermail']);
            $pwd      = trim($form['pwd']);
            NoCSRF::check('csrf_login', $form, true, (60 * 10), false);
            if (!filter_var($usermail, FILTER_VALIDATE_EMAIL)) {
                $msg->error(Messages::MESSAGE_INVALID_EMAIL);
            }
            /** @var User $row */
            $row = $em->getRepository(User::class)->findOneBy([
                'usermail' => $usermail,
                'active'   => true,
            ]);
            if ($row) {
                if (password_verify($pwd, $row->getPwd())) {
                    $this->session()->set('Authentication', [
                        'id'          => $row->getId(),
                        'fullname'    => $row->getFullname(),
                        'usermail'    => $row->getUsermail(),
                        'last_access' => $row->getLastAccess(),
                    ]);
                    $row->setLastAccess(new \DateTime());
                    $em->flush();
                    return $response->withStatus(302)->withHeader('Location', self::SUCCESS_REDIRECT);
                } else {
                    $msg->error(Messages::MESSAGE_BAD_PASSWORD);
                }
            } else {
                $msg->error(Messages::MESSAGE_INVALID_USER);
            }
        }
        return $this->smarty('[administrator]login.tpl', [
            'messages' => $msg->display(null, false),
            'token'    => NoCSRF::generate('csrf_login'),
        ]);
    }
}
