<?php
/**
 * This file is part of the Zendy CMS package.
 *
 * (c) Concetto Vecchio <info@cvsolutions.it>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SimpleSkeletonCMS\Controller\Administrator;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use SimpleSkeletonCMS\Controller\AbstractController;
use SimpleSkeletonCMS\Entity\Page;
use SimpleSkeletonCMS\Utility\Messages;

/**
 * Class PagesController
 * @package SimpleSkeletonCMS\Controller\Administrator
 */
class PagesController extends AbstractController
{
    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     */
    public function index(Request $request, Response $response, $args)
    {
        $this->smarty('[administrator]pages.tpl', [
            'pages' => $this->entityManager()->getRepository(Page::class)->findAll(),
        ]);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     */
    public function add(Request $request, Response $response, $args)
    {
        $msg = $this->flash();
        $em  = $this->entityManager();
        if ($request->getMethod() == 'POST') {
            $form  = $request->getParsedBody();
            $today = new \DateTime();

            $page = new Page();
            $page->setParent($form['parent']);
            $page->setTitleIt($form['title_it']);
            $page->setSlugIt($form['slug_it']);
            $page->setContentIt($form['content_it']);
            $page->setMetaTitleIt($form['meta_title_it']);
            $page->setMetaDescriptionIt($form['meta_description_it']);
            $page->setMetaKeywordsIt($form['meta_keywords_it']);
            $page->setTitleEn($form['title_en']);
            $page->setSlugEn($form['slug_en']);
            $page->setContentEn($form['content_en']);
            $page->setMetaTitleEn($form['meta_title_en']);
            $page->setMetaDescriptionEn($form['meta_description_en']);
            $page->setMetaKeywordsEn($form['meta_keywords_en']);
            $page->setTitleDe($form['title_de']);
            $page->setSlugDe($form['slug_de']);
            $page->setContentDe($form['content_de']);
            $page->setMetaTitleDe($form['meta_title_de']);
            $page->setMetaDescriptionDe($form['meta_description_de']);
            $page->setMetaKeywordsDe($form['meta_keywords_de']);
            $page->setTitleFr($form['title_fr']);
            $page->setSlugFr($form['slug_fr']);
            $page->setContentFr($form['content_fr']);
            $page->setMetaTitleFr($form['meta_title_fr']);
            $page->setMetaDescriptionFr($form['meta_description_fr']);
            $page->setMetaKeywordsFr($form['meta_keywords_fr']);
            $page->setTitleEs($form['title_es']);
            $page->setSlugEs($form['slug_es']);
            $page->setContentEs($form['content_es']);
            $page->setMetaTitleEs($form['meta_title_es']);
            $page->setMetaDescriptionEs($form['meta_description_es']);
            $page->setMetaKeywordsEs($form['meta_keywords_es']);
            $page->setLinkExternal($form['link_external']);
            $page->setHomePage(!empty($form['home_page']) ? $form['home_page'] : null);
            $page->setPublication($today);
            $page->setModification($today);
            $page->setSorting(null);
            $page->setActive(!empty($form['active']) ? $form['active'] : null);

            $em->persist($page);
            $em->flush();
            $msg->success(Messages::MESSAGE_CONFIRMATION);
        }
        $this->smarty('[administrator]pages_add.tpl', [
            'parentPages' => $em->getRepository(Page::class)->findBy([
                'homePage' => null,
                'active'   => true,
            ]),
            'messages'    => $msg->display(null, false),
        ]);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     */
    public function edit(Request $request, Response $response, $args)
    {
        $msg = $this->flash();
        $em  = $this->entityManager();
        $ID  = $args['id'];
        $row = $em->getRepository(Page::class)->find($ID);
        if ($request->getMethod() == 'POST') {
            $form  = $request->getParsedBody();
            $today = new \DateTime();

            $row->setParent($form['parent']);
            $row->setTitleIt($form['title_it']);
            $row->setSlugIt($form['slug_it']);
            $row->setContentIt($form['content_it']);
            $row->setMetaTitleIt($form['meta_title_it']);
            $row->setMetaDescriptionIt($form['meta_description_it']);
            $row->setMetaKeywordsIt($form['meta_keywords_it']);
            $row->setTitleEn($form['title_en']);
            $row->setSlugEn($form['slug_en']);
            $row->setContentEn($form['content_en']);
            $row->setMetaTitleEn($form['meta_title_en']);
            $row->setMetaDescriptionEn($form['meta_description_en']);
            $row->setMetaKeywordsEn($form['meta_keywords_en']);
            $row->setTitleDe($form['title_de']);
            $row->setSlugDe($form['slug_de']);
            $row->setContentDe($form['content_de']);
            $row->setMetaTitleDe($form['meta_title_de']);
            $row->setMetaDescriptionDe($form['meta_description_de']);
            $row->setMetaKeywordsDe($form['meta_keywords_de']);
            $row->setTitleFr($form['title_fr']);
            $row->setSlugFr($form['slug_fr']);
            $row->setContentFr($form['content_fr']);
            $row->setMetaTitleFr($form['meta_title_fr']);
            $row->setMetaDescriptionFr($form['meta_description_fr']);
            $row->setMetaKeywordsFr($form['meta_keywords_fr']);
            $row->setTitleEs($form['title_es']);
            $row->setSlugEs($form['slug_es']);
            $row->setContentEs($form['content_es']);
            $row->setMetaTitleEs($form['meta_title_es']);
            $row->setMetaDescriptionEs($form['meta_description_es']);
            $row->setMetaKeywordsEs($form['meta_keywords_es']);
            $row->setLinkExternal($form['link_external']);
            $row->setHomePage(!empty($form['home_page']) ? $form['home_page'] : null);
            $row->setModification($today);
            $row->setSorting(null);
            $row->setActive(!empty($form['active']) ? $form['active'] : null);

            $em->flush();
            $msg->success(Messages::MESSAGE_CONFIRMATION);
        }

        $this->smarty('[administrator]pages_edit.tpl', [
            'page'        => $row,
            'messages'    => $msg->display(null, false),
            'parentPages' => $em->getRepository(Page::class)->findBy([
                'homePage' => null,
                'active'   => true,
            ]),
        ]);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return int
     */
    public function sorting(Request $request, Response $response, $args)
    {
        if ($request->getMethod() == 'POST') {
            $form = $request->getParsedBody();
            print_r($form);
            return $response->withStatus(200)->getBody()->write(null);
        }
        $this->smarty('[administrator]pages_sorting.tpl');
    }
}
