<?php
/**
 * This file is part of the Zendy CMS package.
 *
 * (c) Concetto Vecchio <info@cvsolutions.it>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SimpleSkeletonCMS\Controller\Application;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use SimpleSkeletonCMS\Controller\AbstractController;
use SimpleSkeletonCMS\Entity\Page;
use SimpleSkeletonCMS\Utility\Messages;

/**
 * Class PageController
 * @package SimpleSkeletonCMS\Controller\Application
 */
class PageController extends AbstractController
{
    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function index(Request $request, Response $response, $args)
    {
        $lang = $args['lang'];
        $slug = $args['slug'];
        $page = $this->entityManager()->getRepository(Page::class)->findBy([
            'slug' . ucfirst($lang) => $slug,
            'active'                => true,
        ]);
        if (!$page) {
            $response->withStatus(404);
            $response->withHeader('Content-Type', 'text/html');
            $response->getBody()->write(Messages::MESSAGE_403);
            return $response;
        }
        $this->smarty('[application]page.tpl', [
            'navbar' => $this->navbar(0, 1, $lang),
            'page'   => $page,
        ]);
    }
}
