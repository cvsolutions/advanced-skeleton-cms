<?php
/**
 * This file is part of the Zendy CMS package.
 *
 * (c) Concetto Vecchio <info@cvsolutions.it>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SimpleSkeletonCMS\Controller\Application;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use SimpleSkeletonCMS\Controller\AbstractController;

/**
 * Class IndexController
 * @package SimpleSkeletonCMS\Controller\Application
 */
class IndexController extends AbstractController
{
    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     */
    public function index(Request $request, Response $response, $args)
    {
        $this->smarty('[application]index.tpl', []);
    }
}
