<?php
/**
 * This file is part of the Zendy CMS package.
 *
 * (c) Concetto Vecchio <info@cvsolutions.it>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SimpleSkeletonCMS\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class PageRepository
 * @package SimpleSkeletonCMS\Repository
 */
class PageRepository extends EntityRepository
{
    /**
     * @param $parent
     * @param $language
     * @return array
     */
    public function navbar($parent, $language)
    {
        $conn = $this->getEntityManager()->getConnection();
        $stmt = $conn->prepare("SELECT a.id, a.title_{$language}, a.slug_{$language}, a.link_external, Deriv1.Count 
                                FROM cms_pages a 
                                LEFT OUTER JOIN (SELECT parent, COUNT(*) AS Count 
                                                 FROM cms_pages GROUP BY parent) Deriv1 
                                ON a.id = Deriv1.parent 
                                WHERE a.parent = ? 
                                AND a.home_page IS NULL 
                                AND a.active = 1
                                ORDER BY sorting ASC");

        $stmt->execute([$parent]);
        return $stmt->fetchAll();
    }
}
