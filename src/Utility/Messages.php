<?php
/**
 * This file is part of the Zendy CMS package.
 *
 * (c) Concetto Vecchio <info@cvsolutions.it>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SimpleSkeletonCMS\Utility;

/**
 * Class Messages
 * @package SimpleSkeletonCMS\Utility
 */
class Messages
{
    const MESSAGE_403           = 'Questa sezione del sito è ad accesso riservato, effettua il login per accedere.';
    const MESSAGE_404           = 'La pagina che cercavi non esiste più';
    const MESSAGE_CONFIRMATION  = 'Operazione completata con successo';
    const MESSAGE_BAD_PASSWORD  = 'La Password digitata non è valida';
    const MESSAGE_INVALID_USER  = 'Non esiste un utente con questo nome';
    const MESSAGE_INVALID_EMAIL = 'Indirizzo email non valido';
}
