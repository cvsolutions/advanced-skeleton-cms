<?php
/**
 * This file is part of the Zendy CMS package.
 *
 * (c) Concetto Vecchio <info@cvsolutions.it>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Dotenv\Dotenv;
use SimpleSkeletonCMS\Middleware\AuthMiddleware;
use SimpleSkeletonCMS\Middleware\SessionMiddleware;
use SimpleSkeletonCMS\Middleware\TranslationMiddleware;
use Slim\App;
use Slim\Container;

setlocale(LC_TIME, 'it_IT');
error_reporting(E_ALL);
ini_set('display_errors', 1);

require __DIR__ . '/vendor/autoload.php';

$dotenv = new Dotenv(__DIR__);
$dotenv->load();

$settings  = require __DIR__ . '/config/settings.php';
$container = new Container($settings);
$app       = new App($container);
$app->add(new SessionMiddleware(['name' => 'ZendyCMS']));

$app->map(['GET', 'POST'], '/login', '\SimpleSkeletonCMS\Controller\Administrator\LoginController:index');

$app->group('/admin', function () {
    $this->get('', '\SimpleSkeletonCMS\Controller\Administrator\LoginController:index');
    $this->get('/logout', '\SimpleSkeletonCMS\Controller\Administrator\LogoutController:index');
    $this->get('/dashboard', '\SimpleSkeletonCMS\Controller\Administrator\DashboardController:index');
    $this->get('/pages', '\SimpleSkeletonCMS\Controller\Administrator\PagesController:index');
    $this->map(['GET', 'POST'], '/pages/add', '\SimpleSkeletonCMS\Controller\Administrator\PagesController:add');
    $this->map(['GET', 'POST'], '/pages/edit/{id:[0-9]+}', '\SimpleSkeletonCMS\Controller\Administrator\PagesController:edit');
    $this->map(['GET', 'POST'], '/pages/sorting', '\SimpleSkeletonCMS\Controller\Administrator\PagesController:sorting');
})->add(new AuthMiddleware());

$app->group('/', function () {
    $this->get('[{lang}]', '\SimpleSkeletonCMS\Controller\Application\IndexController:index');
    $this->get('{lang}/{slug:[a-zA-Z][a-zA-Z0-9_-]*}', '\SimpleSkeletonCMS\Controller\Application\PageController:index');
})->add(new TranslationMiddleware($container));

$app->run();
