{extends file="layout_administrator.tpl"}
{assign var="activeNav" value="2"}
{block name="body"}
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Pagine</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="btn-group" role="group" aria-label="...">
                        <a href="/admin/pages/add" class="btn btn-default"><i class="fa fa-plus-circle" aria-hidden="true"></i> Aggiungi pagina</a>
                        <a href="/admin/pages/sorting" class="btn btn-default"><i class="fa fa-bars" aria-hidden="true"></i> Ordinamento</a>
                    </div>
                </div>
                <div class="panel-body">
                    <table data-toggle="table"
                           data-show-toggle="true"
                           data-show-columns="true"
                           data-mobile-responsive="true"
                           data-search="true"
                           data-pagination="true"
                           data-sort-name="id"
                           data-sort-order="desc">
                        <thead>
                        <tr>
                            <th data-field="id" data-sortable="true">ID</th>
                            <th data-field="title" data-sortable="true">Pagina</th>
                            <th data-field="home" data-sortable="true">Home</th>
                            <th data-field="edit" data-sortable="true">Modifica</th>
                            <th data-field="action" data-sortable="false">Operazioni</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach from=$pages item=row}
                            <tr>
                                <td>{$row->getId()}</td>
                                <td>{$row->getTitleIt()}</td>
                                <td>{$row->isHomePage()}</td>
                                <td>{$row->getModification()|date_format}</td>
                                <td>
                                    <a href="/admin/pages/edit/{$row->getId()}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-camera" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-trash text-danger" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
{/block}
