$(function () {
    $('#sortable').sortable({
        placeholder: 'li',
        opacity: 0.6,
        cursor: 'move',
        update: function (event, ui) {
            var serialize = $(this).sortable('serialize');
            $.ajax({
                data: serialize,
                type: 'POST',
                dataType: 'json',
                url: '/admin/pages/sorting',
                success: function (data, textStatus, jqXHR) {
                    console.log(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(jqXHR.responseText);
                }
            });
        }
    });

    $('#title_it').stringToSlug({
        getPut: '#slug_it'
    });

    $('#title_en').stringToSlug({
        getPut: '#slug_en'
    });

    $('#title_de').stringToSlug({
        getPut: '#slug_de'
    });

    $('#title_fr').stringToSlug({
        getPut: '#slug_fr'
    });

    $('#title_es').stringToSlug({
        getPut: '#slug_es'
    });

});

!function ($) {
    $(document).on('click', 'ul.nav li.parent > a > span.icon', function () {
        $(this).find('em:first').toggleClass('glyphicon-minus');
    });
    $('.sidebar span.icon').find('em:first').addClass('glyphicon-plus');
    $('[data-toggle="tooltip"]').tooltip();
    $('#js-validate').validate();
}(window.jQuery);

$(window).on('resize', function () {
    if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
});

$(window).on('resize', function () {
    if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
});