var gulp = require('gulp');
var less = require('gulp-less');
var concat = require('gulp-concat');
var cleanCSS = require('gulp-clean-css');
var clean = require('gulp-clean');
var uglify = require('gulp-uglify');

gulp.task('compile-less', function () {
    return gulp.src([
        'bower_components/bootstrap/less/bootstrap.less',
        'bower_components/font-awesome/less/font-awesome.less'
    ])
        .pipe(less())
        .pipe(concat('less.css'))
        .pipe(gulp.dest('.tmp'));
});

gulp.task('minify-css', function () {
    return gulp.src([
        '.tmp/less.css',
        'bower_components/bootstrap-table/src/bootstrap-table.css',
        'bower_components/flag-icons/flags.css',
        'css/styles.css'
    ])
        .pipe(cleanCSS())
        .pipe(concat('styles.min.css'))
        .pipe(gulp.dest('css'));
});

gulp.task('minify-js', function () {
    return gulp.src([
        'bower_components/jquery/dist/jquery.js',
        'bower_components/jquery-ui/jquery-ui.js',
        'bower_components/bootstrap/dist/js/bootstrap.js',
        'bower_components/bootstrap-table/src/bootstrap-table.js',
        'bower_components/bootstrap-table/dist/extensions/mobile/bootstrap-table-mobile.js',
        'bower_components/bootstrap-table/dist/locale/bootstrap-table-it-IT.js',
        'bower_components/jquery-validation/dist/jquery.validate.js',
        'bower_components/jquery-validation/src/localization/messages_it.js',
        'bower_components/speakingurl/speakingurl.min.js',
        'bower_components/jquery.stringtoslug/dist/jquery.stringtoslug.min.js',
        'js/jquery.common.js'
    ])
        .pipe(uglify())
        .pipe(concat('jquery.common.min.js'))
        .pipe(gulp.dest('js'));
});

gulp.task('clean-css', function () {
    return gulp.src('.tmp/less.css', {
        read: false
    })
        .pipe(clean());
});

gulp.task('default', ['compile-less', 'minify-css', 'minify-js']);
