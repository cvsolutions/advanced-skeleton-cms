{extends file="layout_administrator.tpl"}
{assign var="activeNav" value="2"}
{block name="body"}
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Aggiungi pagina</h1>
        </div>
    </div>
    <form action="" method="post" id="js-validate" enctype="application/x-www-form-urlencoded">
        {$messages}
        <div class="row">
            <div class="col-lg-9">
                <div class="panel panel-default">
                    <div class="panel-body tabs">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab-it" data-toggle="tab"><div class="b-flag b-flag_it" data-toggle="tooltip" title="Italiano"></div></a></li>
                            <li><a href="#tab-en" data-toggle="tab"><div class="b-flag b-flag_gb" data-toggle="tooltip" title="English"></div></a></li>
                            <li><a href="#tab-de" data-toggle="tab"><div class="b-flag b-flag_de" data-toggle="tooltip" title="Deutsch"></div></a></li>
                            <li><a href="#tab-fr" data-toggle="tab"><div class="b-flag b-flag_fr" data-toggle="tooltip" title="Français"></div></a></li>
                            <li><a href="#tab-es" data-toggle="tab"><div class="b-flag b-flag_es" data-toggle="tooltip" title="Español"></div></a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="tab-it">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="title_it">Nome Pagina</label>
                                            <input type="text" class="form-control" name="title_it" id="title_it" value="" autofocus="autofocus" required="required">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="slug_it">Permalink</label>
                                            <input type="text" class="form-control" name="slug_it" id="slug_it" value="" autofocus="autofocus" required="required">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="content_it">Text</label>
                                            <textarea class="form-control" name="content_it" id="content_it" rows="10" autofocus="autofocus"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <p>
                                            <a class="btn btn-default" role="button" data-toggle="collapse" href="#advanced_settings_it" aria-expanded="false" aria-controls="">
                                                Impostazioni Avanzate
                                            </a>
                                        </p>
                                    </div>
                                </div>
                                <div class="collapse" id="advanced_settings_it">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="meta_title_it">Meta title</label>
                                                <input type="text" class="form-control" name="meta_title_it" id="meta_title_it" value="" autofocus="autofocus">
                                            </div>
                                            <div class="form-group">
                                                <label for="meta_description_it">Meta description</label>
                                                <textarea class="form-control" name="meta_description_it" id="meta_description_it" autofocus="autofocus"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="meta_keywords_it">Meta keywords</label>
                                                <textarea class="form-control" name="meta_keywords_it" id="meta_keywords_it" autofocus="autofocus"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab-en">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="title_en">Nome Pagina</label>
                                            <input type="text" class="form-control" name="title_en" id="title_en" value="" autofocus="autofocus" required="required">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="slug_en">Permalink</label>
                                            <input type="text" class="form-control" name="slug_en" id="slug_en" value="" autofocus="autofocus" required="required">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="content_en">Text</label>
                                            <textarea class="form-control" name="content_en" id="content_en" rows="10" autofocus="autofocus"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <p>
                                            <a class="btn btn-default" role="button" data-toggle="collapse" href="#advanced_settings_en" aria-expanded="false" aria-controls="">
                                                Impostazioni Avanzate
                                            </a>
                                        </p>
                                    </div>
                                </div>
                                <div class="collapse" id="advanced_settings_en">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="meta_title_en">Meta title</label>
                                                <input type="text" class="form-control" name="meta_title_en" id="meta_title_en" value="" autofocus="autofocus">
                                            </div>
                                            <div class="form-group">
                                                <label for="meta_description_en">Meta description</label>
                                                <textarea class="form-control" name="meta_description_en" id="meta_description_en" autofocus="autofocus"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="meta_keywords_en">Meta keywords</label>
                                                <textarea class="form-control" name="meta_keywords_en" id="meta_keywords_en" autofocus="autofocus"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab-de">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="title_de">Nome Pagina</label>
                                            <input type="text" class="form-control" name="title_de" id="title_de" value="" autofocus="autofocus" required="required">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="slug_de">Permalink</label>
                                            <input type="text" class="form-control" name="slug_de" id="slug_de" value="" autofocus="autofocus" required="required">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="content_de">Text</label>
                                            <textarea class="form-control" name="content_de" id="content_de" rows="10" autofocus="autofocus"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <p>
                                            <a class="btn btn-default" role="button" data-toggle="collapse" href="#advanced_settings_de" aria-expanded="false" aria-controls="">
                                                Impostazioni Avanzate
                                            </a>
                                        </p>
                                    </div>
                                </div>
                                <div class="collapse" id="advanced_settings_de">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="meta_title_de">Meta title</label>
                                                <input type="text" class="form-control" name="meta_title_de" id="meta_title_de" value="" autofocus="autofocus">
                                            </div>
                                            <div class="form-group">
                                                <label for="meta_description_de">Meta description</label>
                                                <textarea class="form-control" name="meta_description_de" id="meta_description_de" autofocus="autofocus"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="meta_keywords_de">Meta keywords</label>
                                                <textarea class="form-control" name="meta_keywords_de" id="meta_keywords_de" autofocus="autofocus"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab-fr">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="title_fr">Nome Pagina</label>
                                            <input type="text" class="form-control" name="title_fr" id="title_fr" value="" autofocus="autofocus" required="required">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="slug_fr">Permalink</label>
                                            <input type="text" class="form-control" name="slug_fr" id="slug_fr" value="" autofocus="autofocus" required="required">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="content_fr">Text</label>
                                            <textarea class="form-control" name="content_fr" id="content_fr" rows="10" autofocus="autofocus"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <p>
                                            <a class="btn btn-default" role="button" data-toggle="collapse" href="#advanced_settings_fr" aria-expanded="false" aria-controls="">
                                                Impostazioni Avanzate
                                            </a>
                                        </p>
                                    </div>
                                </div>
                                <div class="collapse" id="advanced_settings_fr">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="meta_title_fr">Meta title</label>
                                                <input type="text" class="form-control" name="meta_title_fr" id="meta_title_fr" value="" autofocus="autofocus">
                                            </div>
                                            <div class="form-group">
                                                <label for="meta_description_fr">Meta description</label>
                                                <textarea class="form-control" name="meta_description_fr" id="meta_description_fr" autofocus="autofocus"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="meta_keywords_fr">Meta keywords</label>
                                                <textarea class="form-control" name="meta_keywords_fr" id="meta_keywords_fr" autofocus="autofocus"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab-es">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="title_es">Nome Pagina</label>
                                            <input type="text" class="form-control" name="title_es" id="title_es" value="" autofocus="autofocus" required="required">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="slug_es">Permalink</label>
                                            <input type="text" class="form-control" name="slug_es" id="slug_es" value="" autofocus="autofocus" required="required">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="content_es">Text</label>
                                            <textarea class="form-control" name="content_es" id="content_es" rows="10" autofocus="autofocus"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <p>
                                            <a class="btn btn-default" role="button" data-toggle="collapse" href="#advanced_settings_es" aria-expanded="false" aria-controls="">
                                                Impostazioni Avanzate
                                            </a>
                                        </p>
                                    </div>
                                </div>
                                <div class="collapse" id="advanced_settings_es">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="meta_title_es">Meta title</label>
                                                <input type="text" class="form-control" name="meta_title_es" id="meta_title_es" value="" autofocus="autofocus">
                                            </div>
                                            <div class="form-group">
                                                <label for="meta_description_es">Meta description</label>
                                                <textarea class="form-control" name="meta_description_es" id="meta_description_es" autofocus="autofocus"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="meta_keywords_es">Meta keywords</label>
                                                <textarea class="form-control" name="meta_keywords_es" id="meta_keywords_es" autofocus="autofocus"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="parent">Genitore</label>
                            <select name="parent" id="parent" class="form-control" autofocus="autofocus">
                                <option value="0">-</option>
                                {foreach from=$parentPages item=row}
                                    <option value="{$row->getId()}">{$row->getTitleIt()}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="link_external">Link esterno</label>
                            <input type="text" class="form-control" name="link_external" id="link_external" value="" autofocus="autofocus">
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="home_page" id="home_page" value="1"> Home page
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="active" id="active" value="1"> Pubblicazione
                            </label>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-primary">Salva</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
{/block}