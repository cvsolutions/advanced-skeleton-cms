<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Zendy CMS</title>
    <link href="/templates/administrator/assets/css/styles.min.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">Log in</div>
                <div class="panel-body">
                    {$messages}
                    <form role="form" id="js-validate" method="post" action="" enctype="application/x-www-form-urlencoded">
                        <fieldset>
                            <div class="form-group">
                                <input class="form-control"
                                       placeholder="E-mail"
                                       name="usermail"
                                       type="email"
                                       value=""
                                       autofocus="autofocus"
                                       required="required">
                            </div>
                            <div class="form-group">
                                <input class="form-control"
                                       placeholder="Password"
                                       name="pwd"
                                       type="password"
                                       value=""
                                       autofocus="autofocus"
                                       required="required">
                            </div>
                            <input type="hidden" name="csrf_login" value="{$token}">
                            <button type="submit" class="btn btn-primary">Login</button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/templates/administrator/assets/js/jquery.common.min.js"></script>
</body>
</html>