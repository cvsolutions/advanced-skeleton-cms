{extends file="layout_administrator.tpl"}
{assign var="activeNav" value="2"}
{block name="body"}
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Ordinamento pagine</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <ul id="sortable">
                        <li id="id-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit</li>
                        <li id="id-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit</li>
                        <li id="id-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
{/block}
