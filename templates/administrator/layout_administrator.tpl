<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Zendy - Administrator</title>
    <link href="/templates/administrator/assets/css/styles.min.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/admin/dashboard"><span>Zendy</span>CMS</a>
            <ul class="user-menu">
                <li class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-user-circle fa-lg" aria-hidden="true"></i> {$smarty.session.Authentication.fullname}
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#"><i class="fa fa-user-o fa-lg" aria-hidden="true"></i> Profilo</a></li>
                        <li><a href="/admin/logout"><i class="fa fa-power-off fa-lg" aria-hidden="true"></i> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="container-fluid">
    <div class="row">
        <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
            <form role="search" action="" method="get" enctype="application/x-www-form-urlencoded">
                <div class="form-group">
                    <input type="search" name="q" class="form-control" placeholder="Search..." required autofocus="">
                </div>
            </form>
            <ul class="nav menu">
                <li class="{if $activeNav eq 1}active{/if}"><a href="/admin/dashboard"><i class="fa fa-tachometer fa-lg" aria-hidden="true"></i> Dashboard</a></li>
                <li class="{if $activeNav eq 2}active{/if}"><a href="/admin/pages"><i class="fa fa-file-text fa-lg" aria-hidden="true"></i> Pagine</a></li>
                <li role="presentation" class="divider"></li>
                <li><a href="/" target="_blank"><i class="fa fa-eye fa-lg" aria-hidden="true"></i> Anteprima del sito</a></li>
            </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
            {block name="body"}{/block}
        </div>
    </div>
</div>
<script src="/templates/administrator/assets/js/jquery.common.min.js"></script>
</body>
</html>