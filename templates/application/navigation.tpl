{* define the function *}
{function name="navigation" level=0 home=1}
    <ul>
        {if $home eq 1}
            <li><a href="#">Home</a></li>
        {/if}
        {foreach $data as $row}
            <li>
                <a href="{$row.permalink}">{$row.name}</a>
                {if $row.children}
                    {navigation data=$row.children level=$level+1 home=0}
                {/if}
            </li>
        {/foreach}
    </ul>
{/function}

{* run the array through the function *}
{navigation data=$navbar}