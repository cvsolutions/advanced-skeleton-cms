# Simple Skeleton CMS
Simple content management system

- SEO Friendly URLs
- Modular and extensible
- Native multilingual

# Requisiti

- Apache 2.2.x
- PHP >= 5.5
- MySQL / PostgreSQL

#Command CLI

- Creare un nuovo account amministratore
```sh
php config/add-user.php
```

- PHP_CodeSniffer tokenizes PHP, JavaScript and CSS files and detects violations of a defined set of coding standards.
```sh
php vendor/bin/phpcs --standard=PSR2 index.php
```

- Executes (or dumps) the SQL needed to update the database schema to match the current mapping metadata.
```sh
php vendor/bin/doctrine orm:schema-tool:update --force
```
